# nero-swap

Simple Flask based Wownero and Monero swap tool. It works by running both XMR and WOW wallet RPC processes, allowing users to create new swaps with predefined amounts which they submit, then periodically checking for payment. Each swap will create a new `subaddress` in the respective wallet where payments are then checked until the swap expires. When payment is received for 1 coin, the agreed upon amount of the other coin will be dispersed.


## Setup

These instructions were quickly whipped up so I could at least get some basic stuff in place. I haven't tested or run the app in a while so there may be quirks that aren't documented here. Would appreciate any sort of testing and PRs from anyone willing to run it.

You'll need Docker, Docker-Compose, and Make installed.

`sudo apt-get install docker.io docker-compose make`

Setup secrets.

`cp env-example .env && vim .env`

Make up passwords/secrets for everything. Whatever you set initially is what things get created with.

Launch postgres database and redis cache in containers via docker-compose, then execute script in `./bin/prod_wallets` to initialize and launch the XMR and WOW wallets as ad-hoc docker containers:

`make prod-up  # docker-compose up -d && ./bin/prod_wallets`

The wallets will be in interactive mode - you should grab the mnemonic seeds and note the public address of the wallets as they get initialized; type "exit" to move on. That bash script will initialize on the first run and then run the wallet afterwards as long as the provided path in `.env` remains the same and points at an existing wallet directory.

Setup a crontab to execute recurring scripts - process incoming payouts, outgoing payouts and delete expired swaps.

`crontab crontab  # see the "crontab" file in the repo for the content`


Install dependencies via pip and initialize the postgres database via sqlalchemy:

`make prod-up`

Run the app via Gunicorn

`make prod`

Setup Apache/Nginx/etc to host as your webserver and proxy requests back to Gunicorn. Sample configs provided in `conf` directory.

## Maintenance

There is no integration with TradeOgre, operators must manually maintain funds and keep both wallets full.

There is an administration page where operators can access to view the status of all the swaps and see balances of the wallets.

There is an optional integration with Mattermost chat server to receive chat based updates on swap statuses.


* /api/prices - shows current prices from coingecko
* /api/wallets - shows wallet addresses and height
* /stats?token=xxxxx - shows swap status and info, wallet balances, etc
