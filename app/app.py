from app.factory import create_app

# todo - system-wide logging
# todo - system status middleware and maintenance
app = create_app()

if __name__ == '__main__':
    app.run()
