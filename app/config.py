from dotenv import load_dotenv
from secrets import token_urlsafe
from os import getenv


load_dotenv()

# Site meta
SITE_NAME = getenv('SITE_NAME', '*nero Swap')
SECRET_KEY = getenv('SECRET_KEY')
STATS_TOKEN = getenv('STATS_TOKEN', token_urlsafe(8))
SERVER_NAME = getenv('SERVER_NAME', 'localhost:5000')

# Crypto RPC
WOW_WALLET_PASS = getenv('WOW_WALLET_PASS')
WOW_WALLET_RPC_USER = getenv('WOW_WALLET_RPC_USER')
WOW_WALLET_RPC_PASS = getenv('WOW_WALLET_RPC_PASS')
WOW_WALLET_RPC_ENDPOINT = getenv('WOW_WALLET_RPC_ENDPOINT')
WOW_DAEMON_URI = getenv('WOW_DAEMON_URI')
XMR_WALLET_PASS = getenv('XMR_WALLET_PASS')
XMR_WALLET_RPC_USER = getenv('XMR_WALLET_RPC_USER')
XMR_WALLET_RPC_PASS = getenv('XMR_WALLET_RPC_PASS')
XMR_WALLET_RPC_ENDPOINT = getenv('XMR_WALLET_RPC_ENDPOINT')
XMR_DAEMON_URI = getenv('XMR_DAEMON_URI')

# Database
DB_HOST = getenv('DB_HOST', 'localhost')
DB_PORT = getenv('DB_PORT', 5432)
DB_NAME = getenv('DB_NAME', 'neroswap')
DB_USER = getenv('DB_USER', 'neroswap')
DB_PASS = getenv('DB_PASS')

# Redis
REDIS_HOST = getenv('REDIS_HOST', 'localhost')
REDIS_PORT = getenv('REDIS_PORT', 6379)

# Development
TEMPLATES_AUTO_RELOAD = True

# Swap
SWAP_MIN_USD = int(getenv('SWAP_MIN_USD', 1))
SWAP_MAX_USD = int(getenv('SWAP_MAX_USD', 100))
SWAP_FEE_PERCENT = int(getenv('SWAP_FEE_PERCENT', 5))
SWAP_EXPIRATION_HOURS = int(getenv('SWAP_EXPIRATION_HOURS', 12))

# Mattermost
MM_ICON = getenv('MM_ICON', 'https://neroswap.com/static/images/neroswap-logo.png')
MM_CHANNEL = getenv('MM_CHANNEL', 'neroswap')
MM_USERNAME = getenv('MM_USERNAME', 'neroSwap')
MM_ENDPOINT = getenv('MM_ENDPOINT')
